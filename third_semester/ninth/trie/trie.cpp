#include <iostream>
#include <string>
#include <vector>
#include <limits>

class Automata {
 private:
  struct TrieNode {
    static const char start = 'a';
    static const size_t power = 26;
    size_t size = -1;
    TrieNode* next[power]{};
    TrieNode* AddNext(char c);
    ~TrieNode();
  };
 public:
  Automata();
  Automata(const std::vector<std::string>& dict);
  void Clear();
  bool Find(const std::string& str);
  void Insert(const std::string& str);
  ~Automata();
 private:
  TrieNode* root_;
};

int main() {
  std::vector<std::string> dict = {"abcd", "ab", "aed", "abc", "dea"};
  Automata trie(dict);
  std::cout << trie.Find("abc") << std::endl;
  std::cout << trie.Find("bebebe") << std::endl;
  return 0;
}

Automata::TrieNode* Automata::TrieNode::AddNext(char c) {
  int idx = c - start;
  if (!next[idx]) {
    next[idx] = new TrieNode();
  }
  return next[idx];
}

Automata::TrieNode::~TrieNode() {
  for (auto& child : next) {
    delete child;
  }
}

Automata::Automata() : root_(new TrieNode()) {}

Automata::Automata(const std::vector<std::string>& dict) : Automata() {
  for (const auto& s : dict) {
    Insert(s);
  }
}

void Automata::Clear() {
  delete root_;
  root_ = new TrieNode();
}

bool Automata::Find(const std::string& str) {
  TrieNode* cur = root_;
  for (const auto& s : str) {
    int idx = s - TrieNode::start;
    if (!cur->next[idx]) {
      return false;
    }
    cur = cur->next[idx];
  }
  return cur->size != std::numeric_limits<size_t>::max();
}

void Automata::Insert(const std::string& str){
  TrieNode* cur = root_;
  for (const auto& symb: str) {
    cur = cur->AddNext(symb);
  }
  cur->size = str.size();
}

Automata::~Automata() {
  delete root_;
}
