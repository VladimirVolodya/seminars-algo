#include <iostream>
#include <vector>
#include <string>

class Automaton {
 private:
  struct TrieNode {
    static const char start = 'a';
    static const size_t power = 26;
    size_t size = -1;
    TrieNode* children[power] = {};
    TrieNode* next[power] = {};
    TrieNode* suf_link = nullptr;
    TrieNode* comp_suf_link = nullptr;
    TrieNode* parent;
    char to_parent;
    TrieNode();
    TrieNode(TrieNode* parent, char to_parent);
    TrieNode* AddNext(char symbol);
    TrieNode* GetSufLink();
    TrieNode* GetParent();
    TrieNode* GetNext(char c);
    TrieNode* GetCompSufLink();
    ~TrieNode();
  };
 public:
  struct Occurrence {
    size_t start;
    size_t size;
  };
  
  Automaton();
  Automaton(const std::vector<std::string>& dict);
  std::vector<Occurrence> FindOccurrences(const std::string& text);
  void Insert(const std::string& str);
  ~Automaton();
 private:
  TrieNode* root_;
};

int main() {
  std::vector<std::string> dict = {"abcd", "ab", "aed", "abc", "dea"};
  std::string text = "abcd";
  for (const auto& p : Automaton(dict).FindOccurrences(text)) {
    std::cout << p.start << ' ' << p.size << ' ' << text.substr(p.start, p.size) << std::endl;
  }
  return 0;
}

// implementation

Automaton::TrieNode::TrieNode() : TrieNode(nullptr, -1) {}

Automaton::TrieNode::TrieNode(TrieNode* parent, char to_parent) : parent(parent), to_parent(to_parent) {}

Automaton::TrieNode* Automaton::TrieNode::AddNext(char c) {
  int idx = c - start;
  if (!children[idx]) {
    children[idx] = new TrieNode(this, c);
  }
  return children[idx];
}

Automaton::TrieNode::~TrieNode() {
  for (const auto& p : children) {
    if (p) {
      delete p;
    }
  }
}

std::vector<Automaton::Occurrence> Automaton::FindOccurrences(const std::string& text) {
  std::vector<Occurrence> res;
  auto cur = root_;

  for (size_t i = 0; i < text.size(); ++i) {
    auto tmp = cur;
    while (tmp->size != -1) {
      res.push_back({i - tmp->size, tmp->size});
      if (tmp == root_) {
        break;
      }
      tmp = tmp->GetCompSufLink();
    }
    cur = cur->GetNext(text[i]);
  }
  while (cur->size != -1) {
    res.push_back({text.size() - cur->size, cur->size});
    if (cur == root_) {
      break;
    }
    cur = cur->GetCompSufLink();
  }

  return res;
}

Automaton::TrieNode* Automaton::TrieNode::GetSufLink() {
  if (!this->suf_link) {
    if (!this->parent) {
      this->suf_link = this;
    } else if (!this->parent->parent) {
      this->suf_link = this->parent;
    } else {
      this->suf_link = this->parent->GetSufLink()->GetNext(this->to_parent);
    }
  }
  return this->suf_link;
}

Automaton::TrieNode* Automaton::TrieNode::GetCompSufLink() {
  if (!this->comp_suf_link) {
    if (this->GetSufLink()->size != -1 || !this->GetSufLink()->parent) {
      this->comp_suf_link = this->GetSufLink();
    } else {
      this->comp_suf_link = this->GetSufLink()->GetCompSufLink();
    }
  }
  return this->comp_suf_link;
}

Automaton::TrieNode* Automaton::TrieNode::GetNext(char c) {
  int idx = c - start;
  if (!this->next[idx]) {
    if (this->children[idx]) {
      this->next[idx] = this->children[idx];
    } else if (!this->parent) {
      this->next[idx] = this;
    } else {
      this->next[idx] = this->GetSufLink()->GetNext(c);
    }
  }
  return this->next[idx];
}

Automaton::Automaton() : root_(new TrieNode()) {}

Automaton::Automaton(const std::vector<std::string>& dict) : Automaton() {
  for (const auto& s : dict) {
    Insert(s);
  }
}

void Automaton::Insert(const std::string& str) {
  TrieNode* cur = root_;
  for (const char& c : str) {
    cur = cur->AddNext(c);
  }
  cur->size = str.size();
}

Automaton::~Automaton() {
  delete root_;
}
