#ifndef SEGMENT_TREE_H
#define SEGMENT_TREE_H
#include <vector>
#include <cstddef>

template <typename T>
class SumOperation {
 public:
  static const T neutral = 0;
  T operator()(const T& first_op, const T& second_op) {
    return first_op + second_op;
  }
};

template <typename T, typename Operation = SumOperation<T>>
class SegmentTree {
 private:
  struct Node {
    size_t left;
    size_t right;
    T value;
    T delta;
  };

 public:
  SegmentTree(const std::vector<T>& arr) {
    size_t n = RoundToTwo(arr.size());
    tree_.resize(2 * n - 1);
    for (size_t i = 0; i < n; ++i) {
      tree_[n + i - 1] = {
        i,
        i + 1,
        (i >= arr.size() ? Operation::neutral : arr[i])
      };
    }
    if (n < 2) {
        return;
    }
    for (size_t i = n - 2;; --i) {
      tree_[i] = {
        tree_[LeftChild(i)].left,
        tree_[RightChild(i)].right,
        op_(tree_[LeftChild(i)].value, tree_[RightChild(i)].value)
      };
      if (!i) {
        break;
      }
    }
  }

  T Query(size_t left, size_t right) {
    return InternalQuery(0, left, right);
  }

  void Update(size_t idx, const T& delta) {
    size_t cur = 0;
    do {
      tree_[cur].value += delta;
      if (idx < tree_[LeftChild(cur)].right) {
        cur = LeftChild(cur);
      } else {
        cur = RightChild(cur);
      }
    } while (cur < tree_.size());
  }

 private:
  size_t RoundToTwo(size_t n) {
    size_t res = 1;
    while (res < n) {
      res <<= 1;
    }
    return res;
  }

  size_t LeftChild(size_t idx) {
    return 2 * idx + 1;
  }
  
  size_t RightChild(size_t idx) {
    return 2 * idx + 2;
  }

  T InternalQuery(size_t cur, size_t left, size_t right) {
    if (left >= tree_[cur].right || right <= tree_[cur].left) {
      return Operation::neutral;
    }
    if(left <= tree_[cur].left && right >= tree_[cur].right) {
      return tree_[cur].value;
    }
    return op_(InternalQuery(LeftChild(cur), left, right), InternalQuery(RightChild(cur), left, right));
  }

  std::vector<Node> tree_;
  Operation op_;
};

#endif //SEGMENT_TREE_H
