#include "segment_tree.h"
#include <iostream>



int main() {
  size_t size = 1 + rand() % 99;
  size_t k_tests = 10;

  std::vector<int> arr(size);
  for (auto& v : arr) {
    v = rand() % 1000;
  }
  auto ft = SegmentTree(arr);      

  for (size_t i = 0; i < k_tests; ++i) {
    size_t left = rand() % size;
    size_t right = left + rand() % (size - left);
    size_t u = rand() % (2 * size);
    for (size_t j = 0; j < u; ++j) {
      size_t idx = rand() % size;
      int d = rand() % 100;
      arr[idx] += d;
      ft.Update(idx, d);
    }

    int s = 0;
    for (size_t j = left; j <= right; ++j) {
      s += arr[j];
    }


    std::cout << (s == ft.Query(left, right + 1)) << std::endl;
  }
  return 0;
}
