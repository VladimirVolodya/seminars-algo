#include "segment_tree.h"
#include <iostream>

int main() {
  size_t size = 1 + rand() % 99;
  size_t k_tests = 50;

  std::vector<int> arr(size);
  for (auto& v : arr) {
    v = rand() % 1000;
  }
  auto ft = SegmentTree(arr);

  for (size_t i = 0; i < k_tests; ++i) {
    size_t left = rand() % size;
    size_t right = left + rand() % (size - left);


    size_t k_upd = rand() % size;
    for (size_t i = 0; i < k_upd; ++i) {
      size_t upd_left = rand() % size;
      size_t upd_right = upd_left + rand() % (size - upd_left);
      int d = rand() % 100;
      for (size_t j = upd_left; j <= upd_right; ++j) {
        arr[j] += d;
      }
      ft.Update(upd_left, upd_right + 1, d);
    }

    int s = 0;
    for (size_t j = left; j <= right; ++j) {
      s += arr[j];
    }


    std::cout << (s == ft.Query(left, right + 1)) << std::endl;
  }
  return 0;
}
