#ifndef SEGMENT_TREE_H
#define SEGMENT_TREE_H
#include <vector>
#include <cstddef>
#include <type_traits>

template <typename T>
class SumOperation {
 public:
  static const T neutral = 0;
  T operator()(const T& first_op, const T& second_op) {
    return first_op + second_op;
  }
};

template <typename T, typename Operation, typename = void>
class DefaultAdjOperation {
 public:
  T operator()(const T& first_op, size_t second_op) {
    T res = Operation::neutral;
    T cur = first_op;
    while (second_op) {
      if (second_op & 1ull) {
        res = op_(res, cur);
      }
      cur = op_(cur, cur);
      second_op >>= 1;
    }
    return res;
  }
 private:
  Operation op_;
};

template <typename T, typename Operation>
class DefaultAdjOperation<T, Operation, std::void_t<decltype(std::declval<T>() * std::declval<size_t>())>> {
 public:
  T operator()(const T& first_op, size_t second_op) {
    return first_op * second_op;
  }
 private:
  Operation op_;
};

template <typename T, typename Operation = SumOperation<T>, typename AdjOperation = DefaultAdjOperation<T, SumOperation<T>>>
class SegmentTree {
 private:
  struct Node {
    size_t left;
    size_t right;
    T value;
    T delta;
  };

 public:
  SegmentTree(const std::vector<T>& arr) {
    size_t n = RoundToTwo(arr.size());
    tree_.resize(2 * n - 1);
    for (size_t i = 0; i < n; ++i) {
      tree_[n + i - 1] = {
        i,
        i + 1,
        (i >= arr.size() ? Operation::neutral : arr[i]),
        Operation::neutral
      };
    }
    if (n < 2) {
        return;
    }
    for (size_t i = n - 2;; --i) {
      tree_[i] = {
        tree_[LeftChild(i)].left,
        tree_[RightChild(i)].right,
        op_(tree_[LeftChild(i)].value, tree_[RightChild(i)].value),
        Operation::neutral
      };
      if (!i) {
        break;
      }
    }
  }

  T Query(size_t left, size_t right) {
    return InternalQuery(0, left, right);
  }

  void Update(size_t left, size_t right, const T& delta) {
    InternalUpdate(0, left, right, delta);
  }

 private:
  size_t RoundToTwo(size_t n) {
    size_t res = 1;
    while (res < n) {
      res <<= 1;
    }
    return res;
  }

  size_t LeftChild(size_t idx) {
    return 2 * idx + 1;
  }
  
  size_t RightChild(size_t idx) {
    return 2 * idx + 2;
  }

  T InternalQuery(size_t cur, size_t left, size_t right) {
    if (left >= tree_[cur].right || right <= tree_[cur].left) {
      return Operation::neutral;
    }
    if(left <= tree_[cur].left && right >= tree_[cur].right) {
      return op_(tree_[cur].value, adj_op_(tree_[cur].delta, tree_[cur].right - tree_[cur].left));
    }
    Push(cur);
    return op_(InternalQuery(LeftChild(cur), left, right), InternalQuery(RightChild(cur), left, right));
  }
  
  T InternalUpdate(size_t cur, size_t left, size_t right, const T& delta) {
    if (left >= tree_[cur].right || right <= tree_[cur].left) {
      return Operation::neutral;
    }
    if (left <= tree_[cur].left && right >= tree_[cur].right) {
      tree_[cur].delta = op_(tree_[cur].delta, delta);
      return adj_op_(delta, tree_[cur].right - tree_[cur].left);
    }
    Push(cur);
    T sum_delta = op_(InternalUpdate(LeftChild(cur), left, right, delta), InternalUpdate(RightChild(cur), left, right, delta));
    tree_[cur].value = op_(sum_delta, tree_[cur].value);
    return sum_delta;
  }

  void Push(size_t node) {
    tree_[node].value = op_(adj_op_(tree_[node].delta, tree_[node].right - tree_[node].left), tree_[node].value);
    size_t left_child = LeftChild(node);
    size_t right_child = RightChild(node);
    if (left_child < tree_.size()) {
      tree_[left_child].delta = op_(tree_[node].delta, tree_[left_child].delta);
    }
    if (right_child < tree_.size()) {
      tree_[right_child].delta = op_(tree_[node].delta, tree_[right_child].delta);
    }
    tree_[node].delta = Operation::neutral;
  }

  std::vector<Node> tree_;
  Operation op_;
  AdjOperation adj_op_;
};

#endif //SEGMENT_TREE_H
