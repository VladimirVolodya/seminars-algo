#include <iostream>
#include <unordered_map>
#include <vector>

class SuffixAutomaton {
 private:
  struct Node {
    size_t len;
    size_t suf_link;
    bool terminal;
    std::unordered_map<char, size_t> edges;
    Node() : len(0), suf_link(-1), terminal(false) {
      for (auto c = 'a'; c <= 'z'; ++c) {
        edges[c] = -1;
      }
    }
  };
 public:
  SuffixAutomaton(const std::string& str) {
    size_t last = AddNode();
    for (auto c : str) {
      last = AddChar(c, last);
    }
    while (last != -1) {
      nodes_[last].terminal = true;
      last = nodes_[last].suf_link;
    }

  }
 private:
  std::vector<Node> nodes_;
  size_t AddChar(char c, size_t last) {
    auto new_node = AddNode();
    nodes_[new_node].len = nodes_[last].len + 1;
    nodes_[new_node].suf_link = 0;
    
    while (last != -1 && nodes_[last].edges[c] == -1) {
      nodes_[last].edges[c] = new_node;
      last = nodes_[last].suf_link;
    }

    if (last != -1) {
      auto next = nodes_[last].edges[c];
      if (nodes_[last].len + 1 == nodes_[next].len) {
        nodes_[new_node].suf_link = next;
      } else {
        auto clone = AddNode();
        nodes_[clone] = nodes_[next];
        nodes_[clone].len = nodes_[last].len + 1;
        nodes_[new_node].suf_link = clone;
        nodes_[next].suf_link = clone;
        while (last != -1 && nodes_[last].edges[c] == next) {
          nodes_[last].edges[c] = clone;
          last = nodes_[last].suf_link;
        }
      }
    }
    return new_node;
  }
  size_t AddNode() {
    nodes_.emplace_back();
    return nodes_.size() - 1;
  }
};

int main() {
  SuffixAutomaton a("aaaa");
  return 0;
}
