#include <iostream>
#include <random>
#include <unordered_set>
#include "treap.h"

int main() {
  srand(1431240);
  size_t n = 100;
  std::unordered_set<int> keys;
//   std::vector<std::pair<int, int>> arr;
  while (keys.size() < n) {
    keys.emplace(rand() % 100);
  }
//   for (auto it = keys.begin(); it != keys.end(); ++it) {
//     arr.emplace_back(*it, rand() % 100);
//   }
//   Treap<int, int> treap(arr);
  Treap<int, int> treap;

  for (const int& k : keys) {
    treap.FastInsert(k, rand() % 100);
  }
  std::unordered_set<int>::const_iterator it = keys.cbegin();
  for (size_t i = 0; i < n / 2; ++i, ++it) {
    treap.FastErase(*it);
  }
  for (;it != keys.cend(); ++it) {
    std::cout << treap.Find(*it);
  }
  std::cout << std::endl;
  return 0;
}
