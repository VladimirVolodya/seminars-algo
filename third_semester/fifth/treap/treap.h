#ifndef TREAP_H
#define TREAP_H

#include <vector>
#include <algorithm>

template <typename K, typename V>
class Treap {
 private:
  struct Node;

 public:
  Treap() : root_(nullptr) {};
  Treap(const std::vector<std::pair<K, V>>& arr) : root_(nullptr) {
    std::vector<std::pair<K, V>> arr_cpy = arr;
    if (!std::is_sorted(arr.begin(), arr.end())) {
      std::sort(arr_cpy.begin(), arr_cpy.end());
    }
    Node* last_added = root_;
    for (const auto& p : arr_cpy) {
      while (last_added && last_added->value < p.first) {
        last_added = last_added->parent;
      }

      Node* node = new Node(p.first, p.second);
      if (!last_added) {
        node->left = root_;
        if (root_) {
          root_->parent = node;
        }
        root_ = node;
        continue;
      }
      if (last_added->right) {
        last_added->right->parent = node;
      }

      node->left = last_added->right;
      last_added->right = node;
      node->parent = last_added;
    }
    
  }
  ~Treap() {
    Clear(root_);
  }

  void Insert(const K& key, const V& value) {
    NodePair splitted = Split(root_, key);
    root_ = Merge(Merge(splitted.left_tree, new Node(key, value)), splitted.right_tree);
  }

  void Erase(const K& key) {
    NodePair big_splitted = Split(root_, key);
    Node* ub = UpperBound(big_splitted.right_tree, key);
    if (!ub) {
      root_ = big_splitted.left_tree;
      Clear(big_splitted.right_tree);
      return;
    }
    NodePair small_splitted = Split(big_splitted.right_tree, ub->key);
    root_ = Merge(big_splitted.left_tree, small_splitted.right_tree);
  }

  void FastInsert(const K& key, const V& value) {
    if (!root_) {
      root_ = new Node(key, value);
      return;
    }
    if (root_->value < value) {
      NodePair splitted = Split(root_, key);
      Node* new_node = new Node(key, value, splitted.left_tree, splitted.right_tree, nullptr);
      if (splitted.left_tree) {
        splitted.left_tree->parent = new_node;
      }
      if (splitted.right_tree) {
        splitted.right_tree->parent = new_node;
      }
      root_ = new_node;
      return;
    }
    Node* parent = nullptr;
    Node* node = root_;
    do {
      parent = node;
      if (node->key < key) {
        node = node->right;
      } else if (node->key > key) {
        node = node->left;
      }
    } while (node && node->value > value);
    NodePair splitted = Split(node, key);
    Node* new_node = new Node(key, value, splitted.left_tree, splitted.right_tree, parent);
    if (splitted.left_tree) {
      splitted.left_tree->parent = new_node;
    }
    if (splitted.right_tree) {
      splitted.right_tree->parent = new_node;
    }
    if (parent->key > new_node->key) {
        parent->left = new_node;
    } else {
        parent->right = new_node;
    }
  }

  void FastErase(const K& key) {
    Node* node = root_;
    Node* merged = nullptr;
    while (node) {
      if (node->key < key) {
        node = node->right;
      } else if (node->key > key) {
        node = node->left;
      } else {
        break;
      }
    }
    if (!node) {
      return;
    }
    merged = Merge(node->left, node->right);
    if (!node->parent) {
      root_ = merged;
      delete node;
      return;
    }
    if (node->parent->left == node) {
      node->parent->left = merged; 
    } else {
      node->parent->right = merged;
    }
    if (merged) {
      merged->parent = node->parent;
    }
    delete node;
  }

  bool Find(const K& key) {
    Node* node = root_;
    while (node) {
      if (node->key > key) {
        node = node->left;
      } else if (node->key < key) {
        node = node->right;
      } else {
        break;
      }
    }
    return node != nullptr;
  }

 private:
  struct Node {
    K key;
    V value;
    Node* left;
    Node* right;
    Node* parent;
    Node(const K& key, const V& value, Node* left, Node* right, Node* parent) :
        key(key), value(value), left(left), right(right), parent(parent) {}
    Node(const K& key, const V& value) : Node(key, value, nullptr, nullptr, nullptr) {}
  };
  struct NodePair {
    Node* left_tree;
    Node* right_tree;
  };
  Node* root_;
  
  Node* Merge(Node* left_tree, Node* right_tree) {
    if (!left_tree) {
      return right_tree;
    }
    if (!right_tree) {
      return left_tree;
    }
    if (right_tree->value > left_tree->value) {
      Node* merged = Merge(left_tree, right_tree->left);
      merged->parent = right_tree;
      right_tree->left = merged;
      return right_tree;
    }
    Node* merged = Merge(left_tree->right, right_tree);
    merged->parent = left_tree;
    left_tree->right = merged;
    return left_tree;
  }
  
  NodePair Split(Node* tree, const K& x) {
    if (!tree) {
      return {nullptr, nullptr};
    }
    if (tree->key < x) {
      NodePair splitted = Split(tree->right, x);
      tree->right = splitted.left_tree;
      if (splitted.left_tree) {
        splitted.left_tree->parent = tree;
      }
      if (splitted.right_tree) {
        splitted.right_tree->parent = nullptr;
      }
      return {tree, splitted.right_tree};
    }
    NodePair splitted = Split(tree->left, x);
    tree->left = splitted.right_tree;
    if (splitted.right_tree) {
      splitted.right_tree->parent = tree;
    }
    if (splitted.left_tree) {
      splitted.left_tree->parent = nullptr;
    }
    return {splitted.left_tree, tree};
  }

  Node* UpperBound(Node* node, const K& key) {
    while (node) {
      if (node->key < key) {
        node = node->right;
      } else if (node->key > key) {
        node = node->left;
      } else {
        break;
      }
    }
    if (!node) {
      return nullptr;
    }
    if (!node->right) {
      if (!node->parent || node->parent->left != node) {
        return nullptr;
      }
      return node->parent;
    }
    node = node->right;
    while (node->left) {
      node = node->left;
    }
    return node;
  }

  void Clear(Node* node) {
    if (!node) {
      return;
    }
    Clear(node->left);
    Clear(node->right);
    delete node;
  }
};

#endif // TREAP_H
