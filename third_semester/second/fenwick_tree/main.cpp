#include "fenwick_tree.h"
#include <random>
#include <vector>
#include <iostream>

/*

f(x) = x & (x + 1)
00 -> f(00) = 00 = 0
01 -> f(01) = 00 = 0
10 -> f(10) = 10 = 2
11 -> f(11) = 00 = 0


array:
6 7 5 3
fenwick array:
6 13 5 21

update (3, +90) &&
array:
6 7 5 93
fenwick array:
6 13 5 111

*/



int main() {
  size_t size = 1 + rand() % 99;
  size_t k_tests = 10;

  std::vector<int> arr(size);
  for (auto& v : arr) {
    v = rand() % 1000;
  }
  auto ft = FenwickTree(arr);      

  for (size_t i = 0; i < k_tests; ++i) {
    size_t left = rand() % size;
    size_t right = left + rand() % (size - left);
    size_t u = rand() % (2 * size);
    for (size_t j = 0; j < u; ++j) {
      size_t idx = rand() % size;
      int d = rand() % 100;
      arr[idx] += d;
      ft.Update(idx, d);
    }

    int s = 0;
    for (size_t j = left; j <= right; ++j) {
      s += arr[j];
    }

    std::cout << (s == ft.Query(left, right)) << std::endl;
  }
  return 0;
}
