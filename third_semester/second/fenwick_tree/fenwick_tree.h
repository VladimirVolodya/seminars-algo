#ifndef FENWICK_TREE_H
#define FENWICK_TREE_H
#include <vector>

template <typename T>
class FenwickTree {
 public:
  FenwickTree(const std::vector<T>& that_arr) : fenwick_(that_arr.size()) {
    std::vector<T> pref(that_arr.size() + 1);
    pref.front() = 0;
    for (std::size_t i = 1; i < pref.size(); ++i) {
      pref[i] = pref[i - 1] + that_arr[i - 1];
    }
    fenwick_.front() = that_arr[1];
    for (std::size_t k = 0; k < fenwick_.size(); ++k) {
      fenwick_[k] = pref[k + 1] - pref[f(k)];
    }
  }
  
  void Update(std::size_t idx, const T& value) {
    for (std::size_t i = idx; i < fenwick_.size(); i = g(i)) {
      fenwick_[i] += value;
    }
  }

  T Query(std::size_t left, std::size_t right) {
    return left ? (PrefixQuery(right) - PrefixQuery(left - 1)) : PrefixQuery(right);
  }

 private:
  std::size_t f(std::size_t i) {
    return i & (i + 1);
  }
  std::size_t g(std::size_t i) {
    return i | (i + 1);
  }
  T PrefixQuery(std::size_t r) {
    T sum = 0;
    for (std::size_t i = r;; i = f(i) - 1) {
      sum += fenwick_[i];
      if (!f(i)) {
        break;
      }
    }
    return sum;
  }
 std::vector<T> fenwick_;
};

#endif // FENWICK_TREE_H

