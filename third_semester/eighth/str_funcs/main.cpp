#include <iostream> 
#include <string> 
#include <vector> 

std::vector<size_t> PrefixFunc(const std::string& str) { 
  std::vector<size_t> pref(str.size(), 0); 
  for (size_t i = 1; i < str.size(); ++i) { 
    size_t k = pref[i - 1]; 
    while (k > 0 && str[i] != str[k]) { 
      k = pref[k - 1]; 
    } 
    if (str[k] == str[i]) { 
      ++k; 
    } 
    pref[i] = k; 
  } 
  return pref; 
} 

std::vector<size_t> ZFunction(const std::string& str) { 
  std::vector<size_t> z(str.size(), 0); 
  size_t l = 0; 
  size_t r = 0; 
  for (size_t i = 1; i < str.size(); ++i) {
    z[i] = std::min(r - i, z[i - l]);
    while(i + z[i] < str.size() && str[z[i]] == str[i + z[i]]) { 
      z[i]++; 
    }
    if (i + z[i] >= r) { 
      r = i + z[i]; 
      l = i; 
    }
  } 
  return z; 
}

std::vector<size_t> PrefFromZ(const std::vector<size_t>& z) {
  std::vector<size_t> res(z.size(), 0);
  for (size_t i = 1; i < z.size(); ++i) {
    if (z[i]) {
      for (size_t j = z[i] - 1;; --j) {
        if (res[i + j]) {
          break;
        }
        res[i + j] = j + 1;
        if (!j) {
          break;
        }
      }
    }
  }
  return res;
}

bool KMP(const std::string& pattern, const std::string& str) {
  auto tmp = pattern + "#" + str;
  std::vector<size_t> pref = PrefixFunc(tmp);
  for (size_t i = pattern.size(); i < pref.size(); ++i) {
    if (pref[i] == pattern.size()) {
      return true;
    }
  }
  return false;
}

bool KMP2(const std::string& pattern, const std::string& str) {
  auto tmp = pattern + "#" + str;
  std::vector<size_t> pref(pattern.size(), 0); 
  size_t k = 0;
  for (size_t i = 1; i < tmp.size(); ++i) {
    while (k > 0 && tmp[i] != tmp[k]) { 
      k = pref[k - 1]; 
    }
    if (tmp[k] == tmp[i]) { 
      ++k; 
    }
    if (i < pattern.size()) {
      pref[i] = k;
    }
    if (k == pattern.size()) {
      return true;
    }
  }
  return false; 
}

int main() {
  std::cout << KMP2("abd", "acabac") << std::endl; 
  return 0; 
}