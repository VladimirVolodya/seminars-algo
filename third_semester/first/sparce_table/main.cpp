#include <iostream>
#include <random>
#include "sparce_table.h"

int main() {
  std::size_t n = 10;
  std::size_t k = 10;
  srand(int(time(nullptr)));
  std::vector<int> arr(n);
  for (auto& c : arr) {
    c = rand() % 100;
    std::cout << c << ' ';
  }
  std::cout << std::endl;
  SparceTable st(arr);
  for (std::size_t i = 0; i < k; ++i) {
    size_t l = rand() % n;
    size_t r = std::min(n, l + 1 + rand() % n);
    std::cout << l << ' ' << r << ' ' << st.getMin(l, r) << std::endl;
  }
  return 0;
}
