#ifndef SPACE_TABLE_H
#define SPACE_TABLE_H

#include <vector>

template <typename T>
class MinOperation {
 public:
  T operator()(const T& first, const T& second) {
    return std::min(first, second);
  }
};

template <typename T, typename Operation = MinOperation<T>>
class SparceTable {
 public:
  SparceTable(const std::vector<T>& arr) {
    std::size_t lg = std::__lg(arr.size());
    table_.resize(arr.size());
    for (std::size_t i = 0; i < arr.size(); ++i) {
      table_[i].emplace_back(arr[i]);
    }

    for (std::size_t l = 0; l <= lg; ++l) {
        for (std::size_t i = 0; i + (1ull << (l + 1)) - 1 < arr.size(); ++i) {
            table_[i].emplace_back(op_(table_[i][l], table_[i + (1ull << l)][l]));
        }
    }
  }

  T getMin(std::size_t l, std::size_t r) {
    std::size_t lg = std::__lg(r - l);
    return op_(table_[l][lg], table_[r - (1ull << lg)][lg]);
  }
 private:
  std::vector<std::vector<T>> table_;
  Operation op_;
};

#endif //SPACE_TABLE_H
