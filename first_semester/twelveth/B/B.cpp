#include <iostream>

int** Input(int n) {
  int** buy_times = new int*[n];
  for (int i = n - 1; i > -1; --i) {
    buy_times[i] = new int[3];
    std::cin >> buy_times[i][0] >> buy_times[i][1] >> buy_times[i][2];
  }
  return buy_times;
}

int Solve(int** buy_times, int n) {
  int* res = new int[n];
  int ans = 0;
  res[0] = buy_times[0][0];
  if (n > 1) {
    res[1] = std::min(buy_times[1][1], buy_times[1][0] + res[0]);
    if (n > 2) {
      res[2] = std::min(buy_times[2][2], std::min(buy_times[2][1] + res[0],
                                                  buy_times[2][0] + res[1]));
      for (int i = 3; i < n; ++i) {
        res[i] = std::min(buy_times[i][2] + res[i - 3],
                          std::min(buy_times[i][1] + res[i - 2],
                                   buy_times[i][0] + res[i - 1]));
      }
    }
  }
  ans = res[n - 1];
  delete[] res;
  for (int i = 0; i < n; ++i) {
    delete[] buy_times[i];
  }
  delete[] buy_times;
  return ans;
}

int main() {
  int n = 0;
  std::cin >> n;
  std::cout << Solve(Input(n), n) << std::endl;
  return 0;
}
