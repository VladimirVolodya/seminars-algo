#include <iostream>

bool Solve(int n) {
  bool* first_win = new bool[n + 1];
  bool res = false;
  first_win[0] = false;
  first_win[1] = first_win[2] = true;
  for (int i = 3; i < n + 1; ++i) {
    switch (i % 3) {
      case 0:
        first_win[i] = !first_win[i - 2] || !first_win[i - 1];
        break;
      case 1:
        first_win[i] = !first_win[i - 1] || !first_win[i - 3];
        break;
      default:
        first_win[i] =
            !first_win[i - 1] || !first_win[i - 2] || !first_win[i - 3];
    }
  }
  res = first_win[n];
  delete[] first_win;
  return res;
}

int main() {
  int n = 0;
  std::cin >> n;
  std::cout << (Solve(n) ? 1 : 2) << std::endl;
  return 0;
}