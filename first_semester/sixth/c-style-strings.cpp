#include <cstring>
#include <iostream>

int main() {
  char* str1 = new char[20000];
  char* str2 = new char[10];
  std::cin >> str1;
  char* dest = str2;
  char* src = str1;
  while (*dest++ = *src++)
    ;
  std::cout << str2 << '\n';
  delete[] str1;
  delete[] str2;
  return 0;
}