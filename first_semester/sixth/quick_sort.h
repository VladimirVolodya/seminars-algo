void QuickSort(int* arr, long left, long right);
void Partition(int* arr, long pivot_idx, long left, long right, long& left_eq,
               long& right_eq);
void Swap(int& a, int& b);
