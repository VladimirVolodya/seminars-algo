#include <algorithm>
#include <iostream>

#include "quick_sort.h"

int main() {
  const int arr_size = 18;
  int arr[arr_size];
  int idx = 0;
  int val = 9;
  while (idx < arr_size) {
    if (val % 3 == 0) {
      arr[idx++] = val;
      arr[idx++] = val;
      arr[idx++] = val--;
    } else {
      arr[idx++] = val--;
    }
  }
  for (int i = 0; i < arr_size; ++i) {
    std::cout << arr[i] << ' ';
  }
  std::cout << '\n';
  QuickSort(arr, 0, arr_size);
  for (int i = 0; i < arr_size; ++i) {
    std::cout << arr[i] << ' ';
  }
  return 0;
}