#include <iostream>

#include "list.h"

int main() {
  List list;
  Init(list);
  PushFront(list, 0);
  PushFront(list, 1);
  PushFront(list, 2);
  std::cout << PopBack(list) << ' ' << PopBack(list) << ' ' << PopBack(list)
            << ' ' << PopBack(list) << std::endl;
  Clear(list);
}