#include "list.h"

// ListNode
int ListNode::getVal() { return this->val; }
ListNode* ListNode::getPrev() { return this->prev; }
ListNode* ListNode::getNext() { return this->next; }
void ListNode::setVal(const int& val) { this->val = val; }
void ListNode::setPrev(ListNode* node) { this->prev = node; }
void ListNode::setNext(ListNode* node) { this->next = node; }
void ListNode::Push(int val) {
  auto p_new_node = new ListNode;
  p_new_node->val = val;
  if (prev != nullptr) {
    prev->next = p_new_node;
  }
  p_new_node->prev = prev;
  p_new_node->next = this;
  prev = p_new_node;
}

int ListNode::Pop() {
  int val = val;
  if (next != nullptr) {
    next->prev = prev;
  }
  if (prev != nullptr) {
    prev->next = next;
  }
  return val;
}

// List

size_t List::getSize() { return this->size; }
ListNode* List::getHead() { return this->head; }
ListNode* List::getTail() { return this->tail; }
void List::setSize(size_t new_size) { this->size = new_size; }
void List::setHead(ListNode* new_head) { this->head = new_head; }
void List::setTail(ListNode* new_tail) { this->tail = new_tail; }

void List::PushFront(int val) {
  auto p_new_head = new ListNode;
  p_new_head->setVal(val);
  p_new_head->setNext(nullptr);
  p_new_head->setPrev(head);
  ++size;
  if (head != nullptr) {
    head->setNext(p_new_head);
  } else {
    tail = p_new_head;
  }
  head = p_new_head;
}

void List::PushBack(int val) {
  auto p_new_tail = new ListNode;
  p_new_tail->setVal(val);
  p_new_tail->setNext(tail);
  p_new_tail->setPrev(nullptr);
  ++size;
  if (tail != nullptr) {
    tail->setPrev(p_new_tail);
  } else {
    head = p_new_tail;
  }
  tail = p_new_tail;
}

int List::PopFront() {
  if (size == 0) {
    return 0;
  }
  --size;
  auto prev_head = head;
  head = prev_head->getPrev();
  int ret = prev_head->getVal();
  if (head) {
    head->setNext(nullptr);
  } else {
    tail = nullptr;
  }
  delete prev_head;
  return ret;
}

int List::PopBack() {
  if (size == 0) {
    return 0;
  }
  --size;
  auto prev_tail = tail;
  tail = prev_tail->getNext();
  int ret = prev_tail->getVal();
  if (tail) {
    tail->setPrev(nullptr);
  } else {
    head = nullptr;
  }
  delete prev_tail;
  return ret;
}

void List::Clear() {
  while (size > 0) {
    PopBack();
  }
  head = nullptr;
  tail = nullptr;
}

void List::Init() {
  size = 0;
  head = nullptr;
  tail = nullptr;
}
