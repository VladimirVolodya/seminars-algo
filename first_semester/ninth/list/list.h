#ifndef MY_LIST
#define MY_LIST

#include <cstddef>

class ListNode {
 private:
  int val;
  ListNode* prev;
  ListNode* next;

 public:
  int getVal();
  ListNode* getPrev();
  ListNode* getNext();
  void setVal(const int& val);
  void setPrev(ListNode* node);
  void setNext(ListNode* node);
  void Push(int val);
  int Pop();
};

class List {
 private:
  size_t size;
  ListNode* head;
  ListNode* tail;

 public:
  size_t getSize();
  ListNode* getHead();
  ListNode* getTail();
  void setSize(size_t new_size);
  void setHead(ListNode* new_head);
  void setTail(ListNode* new_tail);
  void PushFront(int val);
  void PushBack(int val);
  int PopFront();
  int PopBack();
  void Clear();
  size_t GetSize();
  void Init();
};

#endif  // MY_LIST
