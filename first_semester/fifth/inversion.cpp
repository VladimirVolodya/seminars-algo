#include <iostream>

void f(int n) {
  if (n == 0) {
    return;
  }
  int x;
  std::cin >> x;
  f(n - 1);
  std::cout << x << ' ';
}

int main() {
  int n;
  std::cin >> n;
  f(n);
  std::cout << std::endl;
  return 0;
}
