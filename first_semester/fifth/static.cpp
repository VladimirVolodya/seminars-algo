#include <iostream>

int f() {
  static int s = 5;
  return ++s;
}

int main() {
  std::cout << f() << '\n' << f() << std::endl;
  return 0;
}