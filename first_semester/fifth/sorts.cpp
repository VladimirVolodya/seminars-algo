#include <iostream>

void bubbleSort(int* arr, int size);
void insertionSort(int* arr, int size);
void selectionSort(int* arr, int size);
void swap(int& a, int& b);

int main() {
  int arr1[10] = {4, 1, 6, 7, 10, 2, 3, 8, 9, 5};
  int arr2[10] = {4, 1, 6, 7, 10, 2, 3, 8, 9, 5};
  int arr3[10] = {4, 1, 6, 7, 10, 2, 3, 8, 9, 5};

  bubbleSort(arr1, 10);
  insertionSort(arr2, 10);
  selectionSort(arr3, 10);

  for (int i = 0; i < 10; ++i) {
    std::cout << arr1[i] << ' ';
  }
  std::cout << std::endl;
  for (int i = 0; i < 10; ++i) {
    std::cout << arr2[i] << ' ';
  }
  std::cout << std::endl;
  for (int i = 0; i < 10; ++i) {
    std::cout << arr3[i] << ' ';
  }
  std::cout << std::endl;
  return 0;
}

void bubbleSort(int* arr, int size) {
  bool sorted;
  for (int i = 0; i < size; ++i) {
    sorted = true;
    for (int j = 0; j < size - i - 1; ++j) {
      if (arr[j] > arr[j + 1]) {
        sorted = false;
        swap(arr[j], arr[j + 1]);
      }
    }
    if (sorted) {
      return;
    }
  }
}

void insertionSort(int* arr, int size) {
  for (int sorted_border = 1; sorted_border < size; ++sorted_border) {
    int val = arr[sorted_border];
    int idx = sorted_border;
    while (idx > 0 && arr[idx - 1] > val) {
      arr[idx] = arr[idx - 1];
      --idx;
    }
    arr[idx] = val;
  }
}
void selectionSort(int* arr, int size) {
  for (int i = 0; i < size - 1; ++i) {
    int min_idx = i;
    for (int j = i + 1; j < size; ++j) {
      if (arr[j] < arr[min_idx]) {
        min_idx = j;
      }
    }
    swap(arr[i], arr[min_idx]);
  }
}

void swap(int& a, int& b) {
  int tmp = a;
  a = b;
  b = tmp;
}
