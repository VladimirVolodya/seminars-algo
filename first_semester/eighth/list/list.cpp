#include "list.h"

void PushFront(List& list, int val) {
  auto p_new_head = new ListNode;
  p_new_head->val = val;
  p_new_head->next = nullptr;
  p_new_head->prev = list.head;
  ++list.size;
  if (list.head != nullptr) {
    list.head->next = p_new_head;
  } else {
    list.tail = p_new_head;
  }
  list.head = p_new_head;
}

void PushBack(List& list, int val) {
  auto p_new_tail = new ListNode;
  p_new_tail->val = val;
  p_new_tail->next = list.tail;
  p_new_tail->prev = nullptr;
  ++list.size;
  if (list.tail != nullptr) {
    list.tail->prev = p_new_tail;
  } else {
    list.head = p_new_tail;
  }
  list.tail = p_new_tail;
}

int PopFront(List& list) {
  if (list.size == 0) {
    return 0;
  }
  --list.size;
  auto prev_head = list.head;
  list.head = prev_head->prev;
  int ret = prev_head->val;
  if (list.head) {
    list.head->next = nullptr;
  } else {
    list.tail = nullptr;
  }
  delete prev_head;
  return ret;
}

int PopBack(List& list) {
  if (list.size == 0) {
    return 0;
  }
  --list.size;
  auto prev_tail = list.tail;
  list.tail = prev_tail->next;
  int ret = prev_tail->val;
  if (list.tail) {
    list.tail->prev = nullptr;
  } else {
    list.head = nullptr;
  }
  delete prev_tail;
  return ret;
}

void Clear(List& list) {
  while (list.size > 0) {
    PopBack(list);
  }
  list.head = nullptr;
  list.tail = nullptr;
}

size_t GetSize(List& list) { return list.size; }

void Push(ListNode& list_node, int val) {
  auto p_new_node = new ListNode;
  p_new_node->val = val;
  if (list_node.prev != nullptr) {
    list_node.prev->next = p_new_node;
  }
  p_new_node->prev = list_node.prev;
  p_new_node->next = &list_node;
  list_node.prev = p_new_node;
}

int Pop(ListNode& list_node) {
  int val = list_node.val;
  if (list_node.next != nullptr) {
    list_node.next->prev = list_node.prev;
  }
  if (list_node.prev != nullptr) {
    list_node.prev->next = list_node.next;
  }
  delete &list_node;
  return val;
}

void Init(List& list) {
  list.size = 0;
  list.head = nullptr;
  list.tail = nullptr;
}
