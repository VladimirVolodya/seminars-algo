#include <iostream>

int Simulate(int64_t t, int64_t x, int64_t y);

int main() {
    int64_t n, x, y;
    std::cin >> n >> x >> y;
    int64_t left = -1;
    int64_t right = std::min(x, y) + n / 2 * std::max(x, y) + 1;
    int64_t mid;
    while (right - left > 1) {
      mid = (right + left);
      if (Simulate(mid, x, y) >= n) {
        right = mid;
      } else {
        left = mid;
      }
    }
    std::cout << right << std::endl;
  return 0;
}

int Simulate(int64_t t, int64_t x, int64_t y) {
  int64_t min_print_time = std::min(x, y);
  return (t >= min_print_time) + (t - min_print_time) / x +
         (t - min_print_time) / y;
}
