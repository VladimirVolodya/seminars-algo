#include <iostream>

int BinarySearch(int* arr, int left, int right, int x);

int main() {
  int a[] = {0, 1, 2, 2, 2, 4, 5, 10, 20, 30, 100};
  int left = -1, right = 12;
  int mid;
  int x = 2;
  while (left + 1 < right) {
    mid = left - (left - right) / 2;
    if (a[mid] >= x) {
      // if (a[mid] > x) {
      right = mid;
    } else {
      left = mid;
    }
  }
  int res_idx = right;  // left
  std::cout << res_idx << ' ' << a[res_idx] << std::endl;
  return 0;
}

int LeftBinarySearch(int* arr, int left, int right, int x) {
  int mid;
  while (left + 1 < right) {
    mid = left - (left - right) / 2;
    if (arr[mid] >= x) {
      // if (a[mid] > x) {
      right = mid;
    } else {
      left = mid;
    }
  }
  return right;
}
