#include <cstdint>
#include <iostream>

#include "merge-sort.h"

int main() {
  int32_t arr[20] = {4, 2, 5, 7, 10, 4, 25, 25, 12, 87, 4, 2, 5, 7, 10, 4, 25, 25, 12, 87};
  for (int32_t i = 0; i < 20; ++i) {
    std::cout << arr[i] << ' ';
  }
  int32_t* res = MergeSort(arr, arr + 20);
  std::cout << std::endl;
  for (int32_t i = 0; i < 20; ++i) {
    std::cout << res[i] << ' ';
  }
  std::cout << std::endl;
  delete[] res;
  return 0;
}
