#include <iostream>

template <class T>
void Sort(T* arr, T* end) {
  int size = end - arr;
  for (int i = 0; i < size - 1; ++i) {
    int min_idx = i;
    for (int j = i + 1; j < size; ++j) {
      if (arr[j] < arr[min_idx]) {
        min_idx = j;
      }
    }
    T t = arr[i];
    arr[i] = arr[min_idx];
    arr[min_idx] = t;
  }
}

template <class T>
void Sort(T* arr, T* end) {
  int size = end - arr;
  for (int i = 0; i < size - 1; ++i) {
    int min_idx = i;
    for (int j = i + 1; j < size; ++j) {
      if (arr[j] < arr[min_idx]) {
        min_idx = j;
      }
    }
    T t = arr[i];
    arr[i] = arr[min_idx];
    arr[min_idx] = t;
  }
}

int main() {
  int a[4] = {1, 2, 3, 4};
  int b[3] = {5, 6, 7};
  std::copy_backward(b + 1, b + 3, a + 2);
  for (int i = 0; i < 4; ++i) {
    std::cout << a[i] << ' ';
  }
  std::cout << '\n';
  return 0;
}
