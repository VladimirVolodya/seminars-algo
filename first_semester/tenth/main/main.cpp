#include <algorithm>
#include <iostream>

struct Point {
  int x;
  int y;
};

bool CmpDist(const Point& first_point, const Point& second_point) {
  return first_point.x * first_point.x + first_point.y * first_point.y <
         second_point.x * second_point.x + second_point.y * second_point.y;
}

bool CmpX(const Point& first_point, const Point& second_point) {
  return first_point.x < second_point.x;
}

void Out(Point* arr) {
  for (int i = 0; i < 5; ++i) {
    std::cout << arr[i].x << ' ' << arr[i].y << '\n';
  }
}

int main() {
  Point arr[5] = {{0, 1}, {1, 2}, {-4, 10}, {2, 3}, {-1, 1}};
  Out(arr);
  std::cout << '\n';
  bool (*cmp)(const Point& first_point, const Point& second_point) = CmpDist;
  std::sort(arr, arr + 5, cmp);
  Out(arr);
  std::cout << '\n';
  cmp = CmpX;
  std::sort(arr, arr + 5, cmp);
  Out(arr);
  return 0;
}
