#ifndef AVL_H
#define AVL_H

#include <algorithm>
#include <cstdint>

template <class T>
struct Node {
  T key;
  std::int64_t diff;  // left_hight - right_hight
  Node* parent;
  Node* left;
  Node* right;
};

// @return true if insert successful, else (if key already exists) false
template <class T>
bool Insert(Node<T>* root, const T& key);
template <class T>
bool Delete(Node<T>* root, const T& key);
template <class T>
bool Find(Node<T>& root, const T& key);
template <class T>
void LeftRotate(Node<T>* node);
template <class T>
void RightRotate(Node<T>* node);
template <class T>
void BigLeftRotate(Node<T>* node);
template <class T>
void BigRightRotate(Node<T>* node);

template <class T>
void LeftRotate(Node<T>* node) {
  Node<T>* new_root = node->right;
  new_root->parent = node->parent;
  if (node->parent) {
    if (node->parent->left == node) {
      node->parent->left = new_root;
    } else {
      node->parent->right = new_root;
    }
  }
  node->parent = new_root;
  node->right = new_root->left;
  if (new_root->left) {
    new_root->left->parent = node;
  }
  new_root->left = node;
  node->diff += 1 - std::min(new_root->diff, 0);
  new_root->diff += 1 + std::max(node->diff, 0);
}

template <class T>
void BigRotateLeft(Node<T>* node) {
  RightRotate(node->right);
  LeftRotate(node);
}

#endif  // AVL_H