#include <iostream>

int main() {
  uint32_t n, m;
  std::cin >> n >> m;
  uint32_t value = 0;
  uint32_t diag_no = 0;
  const uint32_t max_diag_no = n + m - 1;
  uint32_t **arr = new uint32_t *[n];
  for (uint32_t i = 0; i < n; ++i) {
    arr[i] = new uint32_t[m];
  }
  uint32_t i = 0;
  uint32_t j = 0;
  while (diag_no < max_diag_no) {
    j = std::min(diag_no, m - 1);
    i = diag_no - j;
    while (j > 0 && i < n - 1) {
      arr[i++][j--] = value++;
    }
    if (j == 0 || i == n - 1) {
      arr[std::min(i, n - 1)][j] = value++;
    }
    ++diag_no;
  }

  for (uint32_t x = 0; x < n; ++x) {
    for (uint32_t y = 0; y < m; ++y) {
      std::cout << arr[x][y] << ' ';
    }
    std::cout << std::endl;
  }
  for (uint32_t x = 0; x < n; ++x) {
    delete[] arr[x];
  }
  delete[] arr;
  return 0;
}