#include <iostream>
#include <ctime>
#include <cstdlib>

int main() {
  std::srand(std::time(nullptr));
  const size_t arr_size = 10;
  int32_t *arr = new int32_t[arr_size];
  for (size_t i = 0; i < arr_size; ++i) {
    arr[i] = std::rand() % arr_size;
  }

  for (size_t i = 0; i < arr_size; ++i) {
    std::cout << arr[i] << ' ';
  }
  std::cout << std::endl;
}
