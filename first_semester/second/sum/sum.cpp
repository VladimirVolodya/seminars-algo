#include <iostream>

int main() {
  int num_of_nums;
  int sum = 0;
  int cur_num;
  std::cin >> num_of_nums;
  for (int i = 0; i < num_of_nums; ++i) {
    std::cin >> cur_num;
    sum += cur_num;
  }
  std::cout << sum << std::endl;
}
