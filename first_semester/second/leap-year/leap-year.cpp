#include <iostream>

int main() {
  int year;
  std::cin >> year;
  int by_4 = year % 4;
  int by_100 = year % 100;
  int by_400 = year % 400;
  if ((1 - 2 * by_4 / (by_4 + 1)) * by_100 + (1 - 2 * by_400 / (by_400 + 1))) {
    std::cout << "YES\n";
  } else {
      std::cout << "NO\n";
  }
  return 0;
}