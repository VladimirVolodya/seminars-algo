#include <iostream>

int main() {
  int64_t a, b;
  std::cin >> a >> b;
  if (a == 0 && b == 0) {
    std::cout << 0 << ' ' << 0 << std::endl;
    return 0;
  }
  if (a == 0) {
    std::cout << 0 << ' ' << 1 << std::endl;
    return 0;
  }
  bool sign = a * b > 0;
  if (a < 0) {
    a = -a;
  }
  if (b < 0) {
    b = -b;
  }
  int64_t ca = (a > b) ? a : b;
  int64_t cb = (a > b) ? b : a;
  while (cb) {
    ca %= cb;
    std::swap(ca, cb);
  }
  std::cout << (!sign ? "-" : "") << a / ca << ' ' << b / ca << std::endl;
  return 0;
}