#include <iostream>

int main() {
    long a, b, c, d;
  std::cin >> a >> b >> c >> d;
  if (a == 0) {
    if (b == 0) {
        std::cout << "INF\n";
    } else {
        std::cout << "NO\n";
    }
  } else if (b % a != 0) {
    std::cout << "NO\n";
  } else if (d - b / a * c == 0) {
    std::cout << "NO\n";
  } else {
    std::cout << - b / a << std::endl;
  }
  return 0;
}