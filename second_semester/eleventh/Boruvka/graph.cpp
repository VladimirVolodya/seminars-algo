#include <iostream>
#include <vector>
#include <limits>

class DSU {
 public:
  DSU(std::size_t n) : preds_(n), ranks_(n, 0) {
    for (std::size_t i = 0; i < n; ++i) {
        preds_[i] = i;
    }
  }
  std::size_t FindPred(std::size_t x) {
    if (preds_[x] == x) {
      return x;
    }
    return preds_[x] = FindPred(preds_[x]);
  }
  void Unite(std::size_t x, std::size_t y) {
    if (ranks_[preds_[y]] < ranks_[preds_[x]]) {
      preds_[preds_[y]] = preds_[x];
    } else if (ranks_[preds_[y]] > ranks_[preds_[x]]) {
      preds_[preds_[x]] = preds_[y];
    } else {
        preds_[preds_[x]] = preds_[y];
        ++ranks_[preds_[x]];
    }
  }

 private:
  std::vector<std::size_t> preds_;
  std::vector<std::size_t> ranks_;
};

class Graph {
 private:
  struct Edge;
 public:
  Graph(std::size_t n) {}
  Graph GetBoruvkaMST() {
    DSU dsu(adj_list_.size());
    std::size_t uniq = adj_list_.size();
    Graph res(adj_list_.size());
    while (uniq > 1) {
      std::vector<Edge> min_edges(adj_list_.size(), {-1, std::numeric_limits<std::size_t>::max()});
      for (std::size_t from = 0; from < adj_list_.size(); ++from) {
        for (const auto& edge : adj_list_[from]) {
          std::size_t pred_from = dsu.FindPred(from);
          std::size_t pred_to = dsu.FindPred(edge.to);
          if (pred_from == pred_to) {
            continue;
          }
          if (edge.w < min_edges[pred_from].w) {
            min_edges[pred_from] = edge;
          }
          if (edge.w < min_edges[pred_to].w) {
            min_edges[pred_to] = edge;
          }
        }
      }
      for (std::size_t from = 0; from < adj_list_.size(); ++from) {
        const auto& edge = min_edges[from];
        if (edge.to != -1) {
          dsu.Unite(from, edge.to);
          res.AddEdge(from, edge.to, edge.w);
          --uniq;
        }
      }
    }
    return res;
  }
  void AddEdge(std::size_t from, std::size_t to, std::size_t w) {
    adj_list_[from].emplace_back(to, w);
    adj_list_[to].emplace_back(from, w);
  }
 private:
   struct Edge {
    std::size_t to;
    std::size_t w;
  };
  std::vector<std::vector<Edge>> adj_list_;
};

