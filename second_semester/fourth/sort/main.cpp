#include <iostream>
#include <random>
#include <algorithm>
#include <cmath>

struct Point {
  int x;
  int y;
};

double dist(const Point& first_point, const Point& second_point) {
  return std::sqrt(std::pow(first_point.x - second_point.x, 2) + std::pow(first_point.y - second_point.y, 2));
}

int main() {
  Point pivot = {0, 0};
  Point arr[10];
  for (int i = 0; i < 10; ++i) {
    arr[i].x = rand() % 100;
    arr[i].y = rand() % 100;
  }
  auto f = []() {};
  f();

  static int a = 0;

  std::sort(arr, arr + 10, [&pivot](const Point& first_point, const Point& second_point) {
    return dist(pivot, first_point) < dist(pivot, second_point);
  });

  for (int i = 0; i < 10; ++i) {
    std::cout << arr[i].x << ' ' << arr[i].y << std::endl;
  }

  std::cout << std::endl << pivot.x << std::endl;
  return 0;
}