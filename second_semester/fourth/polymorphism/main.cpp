#include <iostream>

class B;

class A {
 public:
  A() {
    std::cout << "A()\n";
  }
  A(const A&) {
    std::cout << "A(const A&)\n";
  }
  //   A(const B&) {
  //     std::cout << "A(const B&)\n";
  //   }
  virtual void f() {
    std::cout << "A::f()\n";
  }
  virtual ~A() {
    std::cout << "~A()\n";
  }
};

class B : public A {
 public:
  B() {
    std::cout << "B()\n";
  }
  void f() override {
    std::cout << "B::f()\n";
  }
  ~B() {
    std::cout << "~B()\n";
  }
};

int main() {
  A* b = new B;
  b->f();
  delete b;

  std::cout << "============================\n";
  return 0;
}
