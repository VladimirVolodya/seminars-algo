#include <type_traits>
#include <iostream>

template <std::size_t N, std::size_t Low, std::size_t High, std::size_t Mid = (Low + High + 1) / 2>
struct SqrtHelper : SqrtHelper<N, (N < Mid * Mid ? Low : Mid), (N < Mid * Mid ? Mid - 1 : High)> {};

template <std::size_t N, std::size_t Ans>
struct SqrtHelper<N, Ans, Ans, Ans> : std::integral_constant<std::size_t, Ans> {};

template <std::size_t N>
struct Sqrt : SqrtHelper<N, 0, 2'000'000'000>::type {};

template <std::size_t N>
inline constexpr auto kSqrtV = Sqrt<N>::value;


int main() {
  std::cout << kSqrtV<100> << std::endl;
  return 0;
}
