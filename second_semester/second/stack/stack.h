#ifndef MY_STACK
#define MY_STACK

class Stack {
 public:
  void Push(const int& x);
  void Pop();
  const int& Top();
  bool Empty();
  void Clear();
  Stack& operator=(const Stack& that);
  Stack();
  Stack(const Stack& that);
  ~Stack();
  Stack(Stack&& that);
  Stack& operator=(Stack&& that);
  void Swap(Stack& that);

 private:
  struct Node {
    int val;
    Node* next;
  };
  Node* top_;
};

#endif  // MY_STACK