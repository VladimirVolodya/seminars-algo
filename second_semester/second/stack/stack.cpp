#include "stack.h"

void Stack::Push(const int& x) {
  Node* new_top = new Node();
  new_top->val = x;
  new_top->next = top_;
  top_ = new_top;
}

void Stack::Pop() {
  if (top_) {
    Node* prev = top_;
    top_ = top_->next;
    delete prev;
  }
}

const int& Stack::Top() {
  if (top_) {
    return top_->val;
  }
  return -1;
}

void Stack::Clear() {
  while (!Empty()) {
    Pop();
  }
}

bool Stack::Empty() { return top_ == nullptr; }

Stack& Stack::operator=(const Stack& that) {
  Stack tmp(that);
  Swap(tmp);
  return *this;
}

Stack::Stack() : top_(nullptr) {}

Stack::Stack(const Stack& that) : top_(nullptr) {
  Stack tmp;
  Node* curr = that.top_;
  while (curr) {
    tmp.Push(curr->val);
    curr = curr->next;
  }
  while (!tmp.Empty()) {
    Push(tmp.Top());
    tmp.Pop();
  }
}

Stack::~Stack() { Clear(); }

void Stack::Swap(Stack& that) {
  Node* tmp = that.top_;
  that.top_ = top_;
  top_ = tmp;
}

Stack::Stack(Stack&& that) : top_(that.top_) { that.top_ = nullptr; }

Stack& Stack::operator=(Stack&& that) {
  Clear();
  top_ = that.top_;
  that.top_ = nullptr;
  return *this;
}
