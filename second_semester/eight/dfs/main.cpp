#include <vector>

class Graph {
 public:
  void Read() {
    // TODO: implement
  }
  std::vector<std::vector<std::vector<std::size_t>>> GetDFS() {
    std::vector<std::vector<std::vector<std::size_t>>> cond;
    std::vector<Colors> colors(adj_list_.size(), WHITE);
    DFSVisit(cond, colors, 0);
    return cond;
  }
  std::vector<std::size_t>& GetNeighbours(std::size_t vertex) {
    return adj_list_[vertex];
  }

 private:
  enum Colors { WHITE, BLACK, GREY };
  void DFSVisit(std::vector<std::vector<std::vector<std::size_t>>>& cond, std::vector<Colors>& colors,
                std::size_t vertex) {
    if (colors[vertex] == BLACK) {
      return;
    }
    cond.emplace_back();
    colors[vertex] = GREY;
    for (auto neighbour : GetNeighbours(vertex)) {
      if (colors[neighbour] == WHITE) {
        cond.back()[vertex].push_back(neighbour);
        cond.back()[neighbour].push_back(vertex);
        DFSVisit(cond, colors, neighbour);
      }
    }
    colors[vertex] = BLACK;
  }

  std::vector<std::vector<std::size_t>> adj_list_;
};
