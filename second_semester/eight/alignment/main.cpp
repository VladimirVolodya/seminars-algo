#include <iostream>

struct alignas(4) A {
  int i;
  long l;
  char c;
};

int main() {
  std::cout << sizeof(A) << ' ' << alignof(A) << std::endl;
  return 0;
}
