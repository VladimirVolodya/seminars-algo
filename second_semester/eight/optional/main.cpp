#include <cstddef>

template <typename T>
class Optional {
 public:
  Optional() : value_(new std::byte[sizeof(T)]), has_value_(false) {
  }
  ~Optional() {
    Reset();
    delete[] value_;
  }

 private:
  template <typename Args...>
  void Create(Args... args) {
    new (reinterpret_cast<T*>(&value_)) T(std::forward(args)...);
  }
  void Reset() {
    if (has_value_) {
      value_->~T();
    }
  }
  alignas(alignof(T)) std::byte value_[];
  bool has_value_;
};
