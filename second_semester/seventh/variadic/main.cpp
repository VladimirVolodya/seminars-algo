#include <iostream>
#include <memory>

template <typename T>
void f(T&& x) {
  std::cout << std::is_lvalue_reference_v<T&&> << ' ' << std::is_rvalue_reference_v<T&&> << std::endl;
}

template <typename T>
class C {
 public:
  void f(T&&) {
    std::cout << std::is_lvalue_reference_v<T&&> << ' ' << std::is_rvalue_reference_v<T&&> << std::endl;
  }
};

template <typename... Args>
void g(Args... args) {
  std::cout << "ok\n";
}

class A {
 public:
  A() {
    std::cout << "A()\n";
  }
  A(const A&) {
    std::cout << "A(const A&)\n";
  }
  A(A&&) {
    std::cout << "A(A&&)\n";
  }
  ~A() {
    std::cout << "~A()\n";
  }
};

class B {
 public:
  B(const A&) {
    std::cout << "B(const A&)\n";
  }
  B(const A&, const A&) {
    std::cout << "B(const A&, const A&)\n";
  }
  B(A&&) {
    std::cout << "B(A&&)\n";
  }
};

B MakeBSingle(A&& a) {
  return B(a);
}

B MakeBSingleForward(A&& a) {
  return B(std::forward<A>(a));
}

template <typename... Args>
B MakeB(Args&&... args) {
  return B(args...);
}

template <typename... Args>
B MakeBForward(Args&&... args) {
  return B(std::forward<Args>(args)...);
}

template <typename T, typename... Args>
std::unique_ptr<T> MakeUnique(Args... args) {
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}

int main() {
  int&& y = 0;
  int& z = y;
  ++y;
  ++z;
  std::cout << z << std::endl;
  std::cout << "=====\n";
  int x;
  f(x);
  f(1);
  std::cout << "=====\n";
  C<int> a;
  // a.f(x); CE
  a.f(1);
  std::cout << "=====\n";
  g(1, 2, 3);
  g(1, 2);
  std::cout << "=====\n";
  MakeBSingle(A());
  MakeBSingleForward(A());
  std::cout << "=====\n";
  MakeB(A());
  MakeBForward(A());
  return 0;
}
