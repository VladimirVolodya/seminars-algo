#include <iostream>
#include <vector>
#include <queue>
#include <limits>

using GraphB = std::vector<std::vector<int>>;

class Graph {
 public:
  struct Edge {
    size_t to;
    size_t weight;
  };
  Graph(size_t vertices, size_t edges, size_t max_edge) : max_edge_(max_edge) {
    // ...
  }
  const std::vector<Edge>& getNeighbours(size_t vertex) {
    return adj_list_[vertex];
  }
  size_t minRoute(size_t from, size_t to) {
    std::vector<std::queue<size_t>> bfs_qs(max_edge_ * (adj_list_.size() - 1));
    std::vector<size_t> dists(adj_list_.size(), std::numeric_limits<size_t>::max());

    dists[from] = 0;
    bfs_qs.front().emplace(from);
    for (auto& bfs_q : bfs_qs) {
      while (!bfs_q.empty()) {
        size_t cur = bfs_q.front();
        bfs_q.pop();
        for (const auto& neighbour : getNeighbours(cur)) {
          if (dists[neighbour.to] > dists[cur] + neighbour.weight) {
            dists[neighbour.to] = dists[cur] + neighbour.weight;
            bfs_qs[dists[neighbour.to]].emplace(neighbour.to);
          }
        }
      }
    }

    return dists[to];
  }

 private:
  size_t max_edge_;
  std::vector<std::vector<Edge>> adj_list_;
};
