#define CATCH_CONFIG_MAIN
#include "../catch.hpp"

#include <string>
#include <forward_list>
#include <vector>
#include <sstream>

#include "unordered_set.h"
#include "unordered_set.h"  // check include guards

TEST_CASE("DefaultConstructor", "[Constructors]") {
  const UnorderedSet<int> us;
  REQUIRE(us.Size() == 0u);
  REQUIRE(us.Empty());
  REQUIRE(us.BucketCount() == 0u);
  for (int i = 0; i < 10; ++i) {
    REQUIRE_FALSE(us.Find(i));
  }
  for (size_t i = 0u; i < 10u; ++i) {
    REQUIRE(us.BucketSize(i) == 0u);
  }
  REQUIRE(us.LoadFactor() == Approx(0.0f));
}

TEST_CASE("RangeConstructor", "[Constructors]") {
  std::forward_list<int> fl{0, 1, 2, 3};
  const UnorderedSet<int> us(fl.begin(), fl.end());
  REQUIRE(us.Size() == 4u);
  REQUIRE_FALSE(us.Empty());
  REQUIRE(us.BucketCount() == 4u);
  for (int i = 0; i < 10; ++i) {
    const auto bucket = std::hash<int>{}(i) % 4u;
    REQUIRE(us.Bucket(i) == bucket);
    REQUIRE(us.Find(i) == (i < 4));
  }
  size_t bucket_size_sum = 0;
  for (size_t i = 0u; i < 10u; ++i) {
    bucket_size_sum += us.BucketSize(i);
  }
  REQUIRE(bucket_size_sum == 4u);
  REQUIRE(us.LoadFactor() == Approx(1.0f));
}

TEST_CASE("Insert", "[Usage]") {
  UnorderedSet<std::string> us;
  size_t expected_capacity = 1;
  for (size_t i = 0u; i < 10u; ++i) {
    const auto str = std::to_string(i);
    if (i % 2u == 0u) {
      us.Insert(str);
    } else {
      us.Insert(std::move(str));
    }
    if (i + 1 > expected_capacity) {
      expected_capacity *= 2;
    }
    REQUIRE(us.Size() == i + 1);
    REQUIRE_FALSE(us.Empty());
    REQUIRE(us.BucketCount() == expected_capacity);
    REQUIRE(us.LoadFactor() == Approx(static_cast<float>(i + 1) / expected_capacity));
  }
  for (size_t i = 0; i < 10u; ++i) {
    const auto str = std::to_string(i);
    REQUIRE(us.Find(str));
  }
  for (size_t i = 10u; i < 20u; ++i) {
    const auto str = std::to_string(i);
    REQUIRE_FALSE(us.Find(str));
  }
}

TEST_CASE("Rehash", "[Bucket]") {
  UnorderedSet<int> us;
  us.Rehash(11u);

  REQUIRE(us.Size() == 0u);
  REQUIRE(us.Empty());
  REQUIRE(us.BucketCount() == 11u);
  for (int i = 0; i < 10; ++i) {
    const auto bucket = std::hash<int>{}(i) % 11u;
    REQUIRE(us.Bucket(i) == bucket);
    REQUIRE_FALSE(us.Find(i));
  }

  for (int i = 0; i < 10; ++i) {
    us.Insert(i);
  }

  REQUIRE(us.Size() == 10u);
  REQUIRE_FALSE(us.Empty());
  REQUIRE(us.BucketCount() == 11u);
  for (int i = 0; i < 10; ++i) {
    REQUIRE(us.Find(i));
  }
  for (int i = 10; i < 20; ++i) {
    REQUIRE_FALSE(us.Find(i));
  }

  us.Rehash(5u);

  REQUIRE(us.Size() == 10u);
  REQUIRE_FALSE(us.Empty());
  REQUIRE(us.BucketCount() == 11u);
  for (int i = 0; i < 10; ++i) {
    REQUIRE(us.Find(i));
  }
  for (int i = 10; i < 20; ++i) {
    REQUIRE_FALSE(us.Find(i));
  }

  us.Rehash(10u);

  REQUIRE(us.Size() == 10u);
  REQUIRE_FALSE(us.Empty());
  REQUIRE(us.BucketCount() == 10u);
  for (int i = 0; i < 10; ++i) {
    REQUIRE(us.Find(i));
  }
  for (int i = 10; i < 20; ++i) {
    REQUIRE_FALSE(us.Find(i));
  }

  us.Rehash(50u);

  REQUIRE(us.Size() == 10u);
  REQUIRE_FALSE(us.Empty());
  REQUIRE(us.BucketCount() == 50u);
  for (int i = 0; i < 10; ++i) {
    REQUIRE(us.Find(i));
  }
  for (int i = 10; i < 20; ++i) {
    REQUIRE_FALSE(us.Find(i));
  }
}
