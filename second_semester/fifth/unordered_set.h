#ifndef MY_UNORDERED_SET
#define MY_UNORDERED_SET

#include <vector>
#include <list>
#include <functional>

template <typename KeyT, typename HashT = std::hash<KeyT>>
class UnorderedSet {
 public:
  UnorderedSet() : size_(0) {
  }
  template <typename ForwardIterator>
  UnorderedSet(ForwardIterator begin, ForwardIterator end) : size_(0) {
    for (; begin != end; ++begin) {
      Insert(*begin);
    }
  }
  void Insert(const KeyT& key) {
    if (buckets_.empty()) {
      Rehash(1);
    }
    auto& bucket = buckets_[hash(key)];
    if (!findInBucket(bucket, key)) {
      bucket.push_front(key);
      ++size_;
      if (LoadFactor() > max_load_factor_) {
        Rehash(2 * buckets_.size());
      }
    }
  }
  void Rehash(size_t new_cap) {
    if (new_cap == buckets_.size() || static_cast<double>(size_) / static_cast<double>(new_cap) > max_load_factor_) {
      return;
    }
    auto prev_buckets = std::move(buckets_);
    buckets_.resize(new_cap);
    for (auto bucket_iter = prev_buckets.begin(); bucket_iter != prev_buckets.end(); ++bucket_iter) {
      while (!bucket_iter->empty()) {
        auto& bucket = buckets_[hash(bucket_iter->front())];
        bucket.splice_after(bucket.cbefore_begin(), *bucket_iter, bucket_iter->before_begin());
      }
    }
  }
  bool Find(const KeyT& key) const {
    return buckets_.empty() ? false : findInBucket(buckets_[hash(key)], key);
  }
  size_t Size() const {
    return size_;
  }
  size_t BucketCount() const {
    return buckets_.size();
  }
  size_t BucketSize(const KeyT& key) const {
    if (buckets_.empty()) {
      return 0;
    }
    auto& bucket = buckets_[hash(key)];
    return findInBucket(bucket, key) ? std::distance(bucket.cbegin(), bucket.cend()) : 0;
  }
  double LoadFactor() const {
    return buckets_.empty() ? 0 : static_cast<double>(size_) / static_cast<double>(buckets_.size());
  }
  bool Empty() const {
    return !Size();
  }
  size_t Bucket(const KeyT& key) const {
    return buckets_.empty() ? 0 : hash(key);
  }

 private:
  size_t hash(const KeyT& key) const {
    return hash_(key) % buckets_.size();
  }
  bool findInBucket(const std::forward_list<KeyT>& bucket, const KeyT& key) const {
    for (auto iter = bucket.cbegin(); iter != bucket.cend(); ++iter) {
      if (*iter == key) {
        return true;
      }
    }
    return false;
  }
  std::vector<std::forward_list<KeyT>> buckets_;
  static constexpr double max_load_factor_ = 1;
  size_t size_;
  HashT hash_;
};

#endif  // MY_UNORDERED_SET
