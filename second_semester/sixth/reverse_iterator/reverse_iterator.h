#ifndef MY_REVERSE_ITERATOR
#define MY_REVERSE_ITERATOR

#include <iterator>
#include <type_traits>

template <typename Iterator>
class ReverseIterator {
 public:
  using DifferenceType = typename std::iterator_traits<Iterator>::difference_type;
  using ValueType = typename std::iterator_traits<Iterator>::value_type;
  using Pointer = typename std::iterator_traits<Iterator>::pointer;
  using Reference = typename std::iterator_traits<Iterator>::reference;
  using IteratorCategory = typename std::iterator_traits<Iterator>::iterator_category;

  ReverseIterator() = default;
  ReverseIterator(const Iterator& it) : base_(it){};
  Iterator Base() const {
    return base_;
  }
  Reference operator*() const {
    return *(std::prev(base_));
  }
  Pointer operator->() const {
    if constexpr (std::is_pointer_v<Iterator>) {
      return base_ - 1;
    } else {
      return std::prev(base_).operator->();
    }
  }
  Reference operator[](DifferenceType dif) const {
    return *std::prev(base_, dif + 1);
  }
  ReverseIterator& operator++() {
    base_ = std::prev(base_);
    return *this;
  }
  ReverseIterator& operator--() {
    base_ = std::next(base_);
    return *this;
  }
  ReverseIterator operator++(int) {
    auto copy = *this;
    this->operator++();
    return copy;
  }
  ReverseIterator operator--(int) {
    auto copy = *this;
    this->operator--();
    return copy;
  }
  ReverseIterator& operator+=(DifferenceType dif) {
    base_ = std::prev(base_, dif);
    return *this;
  }
  ReverseIterator& operator-=(DifferenceType dif) {
    base_ = std::next(base_, dif);
    return *this;
  }

 private:
  Iterator base_;
};

template <typename Iterator>
bool operator<(const ReverseIterator<Iterator>& left, const ReverseIterator<Iterator> right) {
  return right.Base() < left.Base();
}

template <typename Iterator>
bool operator<=(const ReverseIterator<Iterator>& left, const ReverseIterator<Iterator> right) {
  return right.Base() <= left.Base();
}

template <typename Iterator>
bool operator>(const ReverseIterator<Iterator>& left, const ReverseIterator<Iterator> right) {
  return right.Base() > left.Base();
}

template <typename Iterator>
bool operator>=(const ReverseIterator<Iterator>& left, const ReverseIterator<Iterator> right) {
  return right.Base() >= left.Base();
}

template <typename Iterator>
bool operator==(const ReverseIterator<Iterator>& left, const ReverseIterator<Iterator> right) {
  return right.Base() == left.Base();
}

template <typename Iterator>
bool operator!=(const ReverseIterator<Iterator>& left, const ReverseIterator<Iterator> right) {
  return right.Base() != left.Base();
}

template <typename Iterator>
ReverseIterator<Iterator> operator+(const ReverseIterator<Iterator>& it,
                                    typename ReverseIterator<Iterator>::DifferenceType dif) {
  auto copy = it;
  copy += dif;
  return copy;
}

template <typename Iterator>
ReverseIterator<Iterator> operator+(typename ReverseIterator<Iterator>::DifferenceType dif,
                                    const ReverseIterator<Iterator>& it) {
  return it + dif;
}

template <typename Iterator>
ReverseIterator<Iterator> operator-(const ReverseIterator<Iterator>& it,
                                    typename ReverseIterator<Iterator>::DifferenceType dif) {
  return it + (-dif);
}

template <typename Iterator>
typename ReverseIterator<Iterator>::DifferenceType operator-(const ReverseIterator<Iterator>& left,
                                                             const ReverseIterator<Iterator>& right) {
  return std::distance(left.Base(), right.Base());
}

template <typename Iterator>
ReverseIterator<Iterator> MakeReverseIterator(const Iterator& it) {
  return ReverseIterator<Iterator>(it);
}

template <typename Iterator>
struct std::iterator_traits {
  using difference_type = typename std::iterator_traits<Iterator>::difference_type;
  using valueType = typename std::iterator_traits<Iterator>::value_type;
  using pointer = typename std::iterator_traits<Iterator>::pointer;
  using reference = typename std::iterator_traits<Iterator>::reference;
  using iterator_category = typename std::iterator_traits<Iterator>::iterator_category;
};

#endif  // MY_REVERSE_ITERATOR
