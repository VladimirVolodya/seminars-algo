#ifndef MY_MOVE_ITERATOR
#define MY_MOVE_ITERATOR

#include <iterator>

template <typename Iterator>
class MoveIterator {
 public:
  using DifferenceType = typename std::iterator_traits<Iterator>::difference_type;
  using ValueType = typename std::iterator_traits<Iterator>::value_type;
  using Pointer = typename std::iterator_traits<Iterator>::pointer;
  using Reference =
      std::add_rvalue_reference_t<std::remove_reference_t<typename std::iterator_traits<Iterator>::reference>>;
  using IteratorCategory = typename std::iterator_traits<Iterator>::iterator_category;

  MoveIterator() = default;
  explicit MoveIterator(const Iterator& it) : base_(it){};
  Iterator Base() const {
    return base_;
  }
  Reference operator*() const {
    return std::move(*base_);
  }
  Reference operator[](DifferenceType dif) const {
    return std::move(*std::next(base_, dif));
  }
  MoveIterator& operator++() {
    base_ = std::next(base_);
    return *this;
  }
  MoveIterator& operator--() {
    base_ = std::prev(base_);
    return *this;
  }
  MoveIterator operator++(int) {
    auto copy = *this;
    this->operator++();
    return copy;
  }
  MoveIterator operator--(int) {
    auto copy = *this;
    this->operator--();
    return copy;
  }
  MoveIterator& operator+=(DifferenceType dif) {
    base_ = std::next(base_, dif);
    return *this;
  }
  MoveIterator& operator-=(DifferenceType dif) {
    base_ = std::prev(base_, dif);
    return *this;
  }

 private:
  Iterator base_;
};

template <typename Iterator>
bool operator<(const MoveIterator<Iterator>& left, const MoveIterator<Iterator>& right) {
  return left.Base() < right.Base();
}

template <typename Iterator>
bool operator<=(const MoveIterator<Iterator>& left, const MoveIterator<Iterator>& right) {
  return left.Base() <= right.Base();
}

template <typename Iterator>
bool operator>(const MoveIterator<Iterator>& left, const MoveIterator<Iterator>& right) {
  return left.Base() > right.Base();
}

template <typename Iterator>
bool operator>=(const MoveIterator<Iterator>& left, const MoveIterator<Iterator>& right) {
  return left.Base() >= right.Base();
}

template <typename Iterator>
bool operator==(const MoveIterator<Iterator>& left, const MoveIterator<Iterator>& right) {
  return left.Base() == right.Base();
}

template <typename Iterator>
bool operator!=(const MoveIterator<Iterator>& left, const MoveIterator<Iterator>& right) {
  return left.Base() != right.Base();
}

template <typename Iterator>
MoveIterator<Iterator> operator+(const MoveIterator<Iterator>& it,
                                 typename MoveIterator<Iterator>::DifferenceType dif) {
  auto copy = it;
  copy += dif;
  return copy;
}

template <typename Iterator>
MoveIterator<Iterator> operator+(typename MoveIterator<Iterator>::DifferenceType dif,
                                 const MoveIterator<Iterator>& it) {
  return it + dif;
}

template <typename Iterator>
MoveIterator<Iterator> operator-(const MoveIterator<Iterator>& it,
                                 typename MoveIterator<Iterator>::DifferenceType dif) {
  return it + (-dif);
}

template <typename Iterator>
typename MoveIterator<Iterator>::DifferenceType operator-(const MoveIterator<Iterator>& left,
                                                          const MoveIterator<Iterator>& right) {
  return std::distance(right.Base(), left.Base());
}

template <typename Iterator>
MoveIterator<Iterator> MakeMoveIterator(const Iterator& it) {
  return MoveIterator<Iterator>(it);
}

#endif  // MY_MOVE_ITERATOR