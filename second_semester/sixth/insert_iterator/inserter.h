#ifndef MY_INSERTER
#define MY_INSERTER

template <typename Container>
class BackInserter {};

template <typename Container>
class FrontInserter {};

template <typename Container>
class Inserter {
 public:
  using difference_type = void;
  using value_type = void;
  using pointer = void;
  using reference = void;
  using iterator_category = std::output_iterator_tag;

  using ContainerIterator = Container::const_iterator;
  using ContainerValueType = Container::value_type;

  Inserter(Contaiter& container, ContainerIterator it) : container_(container), it(it) {
  }
  Inserter& operator=(const ContainerValueType& val) {
    container_.insert(it, val);
  }

 private:
  Container& container_;
  ContainerIterator it;
};

#endif  // MY_INSERTER
