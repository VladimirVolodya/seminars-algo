#include <iostream>

class A {
 public:
  explicit A(int val) {
    std::cout << "A(" << val << ")\n";
  }

  A(const A&) {
    std::cout << "A(const A&)\n";
  }

  A& operator=(const A&) {
    std::cout << "operator=(const A&)\n"; 
    return *this;
  }

  ~A() {
    std::cout << "~A()\n";
  }
};

A& f(int val) {
  if (val) {
    A a(1);
    return a;
  }
  A a(-1);
  return a;
}

int main() {
  A a = f(1);
  std::cout << "===============\n";
  return 0;
}
