#include <iostream>

class Transport {
 public:
  virtual void StartEngine() = 0;
  virtual void Ride() = 0;
};

class Car : public Transport {
 public:
  void StartEngine() final { std::cout << "Ключ в зажигании, завели\n"; }
  void Ride() override {
    StartEngine();
    std::cout << "Четыре колеса крутятся от мотора, едем\n";
  }
};

class Truck : public Car {
 public:
  void Ride() override {}
};

class Motocycle : public Transport {
 public:
  void StartEngine() override { std::cout << "Ключ в зажигании, завели\n"; }
  void Ride() override {
    StartEngine();
    std::cout << "Два колеса крутятся от мотора, едем\n";
  }
};

class Bicycle : public Transport {
 public:
  void StartEngine() override{};
  void Ride() override { std::cout << "Крутим педали, едем\n"; }
};

int main() {
  int day_of_week;
  std::cin >> day_of_week;
  Transport *transport;
  if (day_of_week % 3 == 0) {
    transport = new Car;
  } else if (day_of_week % 3 == 1) {
    transport = new Motocycle;
  } else {
    transport = new Bicycle;
  }
  transport->Ride();
  delete transport;
}