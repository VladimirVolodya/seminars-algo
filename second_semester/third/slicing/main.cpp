#include <iostream>

class A {
 public:
  int a = -10;
};

class B : virtual public A {};
class C : virtual public A {};
class D : public B, public C, public A {};

int main() {
  D d;
  std::cout << d.a;
  return 0;
}
