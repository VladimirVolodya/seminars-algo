# Algo Kappa

## Семинарские контесты

1. [Простейшие задачи, ввод-вывод](https://contest.yandex.ru/contest/39872/problems/A/ "Вперед к знаниям!")
2. [Циклы и ветвления](https://contest.yandex.ru/contest/40251/problems/ "Вперед к знаниям!")
3. [Массивы](https://contest.yandex.ru/contest/40457/problems/ "Вперед к знаниям!")
4. [Бинарный поиск и функции](https://contest.yandex.ru/contest/40711/problems/ "Вперед к знаниям!")
5. [Квадратичные сортировки и рекурсия](https://contest.yandex.ru/contest/40878/problems/ "Вперед к знаниям!")
6. Отсутсутствует :(
7. [Знакомство с шабломани](https://contest.yandex.ru/contest/41401/problems/ "Вперед к знаниям!")
8. [Строкии другие приколы](https://contest.yandex.ru/contest/41778/problems/ "Вперед к знаниям!")
9. To be continued...

## Домашние задания

1. [Сортировки, бинпоиск, шаблоны, строки](https://contest.yandex.ru/contest/41104/problems/ "Вперед к оценкам!")

## Полезные материалы с семинаров

### Оглавление

 - [Git](#git)
 - [Санитайзеры](#sanitizers)

<a name="git"></a> 
### Git

#### Установка git'а

В WSL с установленной Ubuntu (или другим дистрибутивом, если устанавливали не по инструкции) выполните
```
sudo apt-get update
sudo apt-get install git
```
Установите свое имя и почту
```
git config --global user.name ”<your_name>”
git config --global user.email <your_email>
```

#### Полезные ссылки

* [Гайд для новичков](https://javarush.ru/groups/posts/2683-nachalo-rabotih-s-git-podrobnihy-gayd-dlja-novichkov "Учим Git")
* [Документация Git](https://git-scm.com/docs "Документация")
* [Браузерная игра, полезная для новичков](https://learngitbranching.js.org/ "Игра началась")

####  Порядок действий для отправки задачи на ревью

##### 1. Создаем репозиторий в [GitLab](https://gitlab.com/ "GitLab"):

![](/images/create_project.png)

Выбираем "*Create blank project*", вписываем имя репозитрия (на скриншоте это *i_love_kappa*), **ОБЯЗАТЕЛЬНО СОЗДАЕМ ПРИВАТНЫЙ РЕПОЗИТОРИЙ**.

![](/images/privacy.png)

##### 2. Клонируем созданный репозиторий себе на машину

Кликаем на большую синюю кнопку *Clone*, копируем ссылку из поля *Clone with HTTPS*

![](/images/link.png)

Переходим в терминал, выполняем 
```
git clone <your_link>
```
У вас спросят логин и пароль GitLab, когда будете вводить пароль не смущайтесь того, что символы не отображаются в терминате. После успешного копирования у вас появится директория, названная также, как вы назвали свой репозиторий при создании. Переходим в нее
```
cd <your_repo_name>
```
##### 3. Создаем ветку и пушим решения.

Убедимся, что находимся на ветке `main`
```
git branch
```
В результате вы должны увидеть вывод `*main`. Создадим ветку для решений. Это можно сделать двумя способами
* Сначала создать ветку, затем перейти на нее
```
git branch solutions
git checkout solutions
```
* Создать и сразу перейти
```
git checkout -b solutions
```
Убедимся, что мы на правильной ветке. Снова выполним
```
git branch
```
Ожидаемый вывод:
```
main
*solutions
```
Копируем свои решения в директорию, в которой находимся (команда `cp`), после чего добавляем файлы в отвлеживаемые git'ом, коммитим и пушим в удаленный репозиторий в созданную ветку (замените `<your_commit_message>` на желаемое сообщение коммита):
```
git add .
git commit -m <your_commit_message>
git push origin solutions
```

##### 4. Добавляем меня в свой репозиторий

В веб-интерфейсе кликаем *Project information* -> *Members* -> *Invite Members*

![](/images/invite.png)

В открывшемся окне вбиваем мой ник (**@VladimirVolodya**), роль выбирайте **Maintainer** (вероятно, достаточно будет и *Developer*, но проверять мы это не будем), даю честное МФТИшное, что не буду портить ваши репозитории. 

![](/images/invitation.png)

##### 5. Создаем мердж реквест

Кликаем в панели слева *Merge requests*, в открывшейся вкладке *New merge request*.

![](/images/new_request.png)

Выбираем в качестве *Source branch* ветку, в которую вы запушили решения, в качестве *Target branch* - ветку `main`. Когда реквест создастся, опционально в меню справа можете поставить меня ревьюером, либо уведомьте меня как-то еще.

<a name="sanitizers"></a> 
### Санитайзеры

Рассмотрим работу address-sanitizer'а (asan'а) на примере следующей программы:
```
    #include <iostream>

    int main() {
    int* arr = new int[100];
    std::cout << arr[100] << std::endl;
    delete[] arr;
    return 0;
    }

```
Здесь видно, что выводим мы элемент, который не является элементом нашего массива. Вообще это UB, но все компилируется и даже работает. Чтобы такие ошибки отловить (а в 99% случаев RE в контесте = ошибка работы с памятью) воспользуемся asan'ом. Скомпилируем программу с ним (`out-of-bounds.cpp` -- название файла, в котором лежит приведенный выше код, флаг `-g` нужен, чтобы мы легко отыскали строку с ошибкой) и запустим результат:
```
g++ out-of-bounds.cpp -fsanitize=address -g -o out-of-bounds
./out-of-bounds
```
Результат:
```
=================================================================
==647365==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x6140000001d0 at pc 0x555fc7684325 bp 0x7ffe940bb770 sp 0x7ffe940bb760
READ of size 4 at 0x6140000001d0 thread T0
    #0 0x555fc7684324 in main /home/vsm/seminars/bad_practices/out-of-bounds.cpp:5
    #1 0x7fb544a2d082 in __libc_start_main ../csu/libc-start.c:308
    #2 0x555fc768420d in _start (/home/vsm/seminars/bad_practices/out-of-bounds+0x120d)

0x6140000001d0 is located 0 bytes to the right of 400-byte region [0x614000000040,0x6140000001d0)
allocated by thread T0 here:
    #0 0x7fb54508e787 in operator new[](unsigned long) ../../../../src/libsanitizer/asan/asan_new_delete.cc:107
    #1 0x555fc76842de in main /home/vsm/seminars/bad_practices/out-of-bounds.cpp:4
    #2 0x7fb544a2d082 in __libc_start_main ../csu/libc-start.c:308

SUMMARY: AddressSanitizer: heap-buffer-overflow /home/vsm/seminars/bad_practices/out-of-bounds.cpp:5 in main
Shadow bytes around the buggy address:
  0x0c287fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff8000: fa fa fa fa fa fa fa fa 00 00 00 00 00 00 00 00
  0x0c287fff8010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c287fff8020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c287fff8030: 00 00 00 00 00 00 00 00 00 00[fa]fa fa fa fa fa
  0x0c287fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8060: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8070: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c287fff8080: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07 
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
  Shadow gap:              cc
==647365==ABORTING
```
Здесь мы видим все, что нас интересует и даже больше. Например, в первой строке видим, что наша ошибка - это `heap-buffer-overflow`, а значит мы где-то не туда пишем / читаем. В этой же строке видим `READ`, ищем английско-русский словарь и понимаем, что именно читаем. По следующей строке понимаем, в каком файле и в какой строке ошибка `.../out-of-bounds.cpp:5`. Далее видим данные о месте, в котором память выделялась память под массив, какие адреса были зарезервированы нашей программой и даже как выглядела оперативная память в окрестности байта, который мы ошибочно прочитали.

 - [Здесь](https://gcc.gnu.org/onlinedocs/gcc/Instrumentation-Options.html, "Санитайзим") можно почитать про доступные санитайзеры (советую `asan` и `undefined`, поиск по слову `-fsanitize`)
 - [Здесь](https://linux.die.net/man/1/g++ "man g++") можно почитать мануал к компилятору. Еще доступ к нему можно получить, выполнив в консоли команду `man g++`.

Собирать программу можно и с несколькими санитайзерами. Пример:
```
g++ out-of-bounds.cpp -fsanitize=address -fsanitize=undefined -g -o out-of-bounds
```

Успехов в ловле ошибок!
